(function(){
    var app = angular.module('main', []);

    app.controller('StoreController', function(){
       this.products = gems;
    });

    var gems = [
        {
            name: 'Rolling Spider',
            image: 'images/dron3.jpg',
            adres: 'details0.html',
            bracket: 'bracket0.html',
            category: 'tanie',
            price: 599.99,
            summary: 'Opis drona1',
            description: 'Opis drona1 długie'
        },
        {
            name: 'Phantom 2',
            image: 'images/dron2m.jpg',
            adres: 'details1.html',
            bracket: 'bracket1.html',
            category: 'tanie',
            price: 799.99,
            summary: 'Opis drona2',
            description: 'Opis drona2 długie'
        },
        {
            name: 'AR DRONE 2',
            image: 'images/dron4m.jpg',
            adres: 'details2.html',
            bracket: 'bracket2.html',
            category: 'średnie',
            price: 999.99,
            summary: 'Opis drona3',
            description: 'Opis drona3 długie'
        },
        {
            name: 'REELY 650 QC09',
            image: 'images/dron5m.jpg',
            adres: 'details3.html',
            bracket: 'bracket3.html',
            category: 'drogie',
            price: 1499.99,
            summary: 'Opis drona4',
            description: 'Opis drona4 długie'
        },
        {
            name: 'DJI Inspire 1',
            image: 'images/dron6m.jpg',
            adres: 'details4.html',
            bracket: 'bracket4.html',
            category: 'drogie',
            price: 1999.99,
            summary: 'Opis drona5',
            description: 'Opis drona5 długie'
        },
        {
            name: 'Wynajem',
            image: 'images/dron1.jpg',
            adres: 'details5.html',
            bracket: 'bracket5.html',
            category: 'uslugi',
            price: 99.99,
            summary: 'Wynajem drona',
            description: 'Wynajem drona długie'
        }
    ];
})();
